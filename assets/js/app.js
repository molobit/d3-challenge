let activeX = 'poverty';
let activeY = 'obesity';
let plot;

d3.csv('assets/data/data.csv').then(data => {
    plot = initPlot('plot', data);

    const xLinearScale = xScale(plot, activeX);
    const yLinearScale = yScale(plot, activeY);

    const bottomAxis = d3.axisBottom(xLinearScale);
    const leftAxis = d3.axisLeft(yLinearScale);


    // append x axis
    plot.xAxis = plot.chartGroup.append("g")
        .classed("x-axis", true)
        .attr("transform", `translate(0, ${plot.chartArea.height})`)
        .call(bottomAxis);

    // append y axis
    plot.yAxis = plot.chartGroup.append("g")
        .call(leftAxis);

    // append initial circles
    plot.circlesGroup = plot.chartGroup.selectAll("circle")
        .data(plot.data)
        .enter()
        .append("circle")
        .attr("cx", d => xLinearScale(d[activeX]))
        .attr("cy", d => yLinearScale(d[activeY]))
        .attr("r", 20)
        .attr("fill", "pink")
        .attr("opacity", ".5");

    setLabels(plot);

    // updateToolTip function above csv import
    updateToolTip(plot, activeX, activeY);



}).catch(error => {
    console.error(error);
});

function initPlot(elementId, data) {

    const svgContainer = {
        height: 800,
        width: 800
    };

    const margin = {
        top: 50,
        right: 50,
        bottom: 100,
        left: 100
    };

    const chartArea = {
        height: svgContainer.height - margin.top - margin.bottom,
        width: svgContainer.width - margin.left - margin.right
    };

    const svg = d3.select(`#${elementId}`)
        .append('svg')
        .attr('height', svgContainer.height)
        .attr('width', svgContainer.width)
    ;

    const chartGroup = svg.append("g")
        .attr("transform", `translate(${margin.left}, ${margin.top})`);

    return {chartGroup, svg, data: parseData(data), chartArea, margin};
}

function xScale(plot, chosenXAxis) {
    const xLinearScale = d3.scaleLinear()
        .domain([d3.min(plot.data, d => d[chosenXAxis]) * 0.8,
          d3.max(plot.data, d => d[chosenXAxis]) * 1.2
        ])
        .range([0, plot.chartArea.width]);

  return xLinearScale;
}

function yScale(plot, chosenYAxis) {
  const yLinearScale = d3.scaleLinear()
    .domain([d3.min(plot.data, d => d[chosenYAxis]) * 0.8,
      d3.max(plot.data, d => d[chosenYAxis]) * 1.2
    ])
    .range([plot.chartArea.height, 0]);

  return yLinearScale;

}

function renderCircles(circlesGroup, newXScale, chosenXAxis, newYScale, chosenYAxis) {

    circlesGroup.transition()
        .duration(1000)
        .attr("cx", d => {
            return newXScale(d[chosenXAxis]);
        })
        .attr("cy", d => {
            return newYScale(d[chosenYAxis]);
        })
    ;

    console.log(circlesGroup);
    return circlesGroup;
}

function parseData(data) {
    data.forEach(entry => {
        entry.poverty     = +entry.poverty;
        entry.povertyMoe  = +entry.povertyMoe;
        entry.age         = +entry.age;
        entry.ageMoe      = +entry.ageMoe;
        entry.income      = +entry.income;
        entry.incomeMoe   = +entry.incomeMoe;
        entry.healthcare  = +entry.healthcare;
        entry.obesity     = +entry.obesity;
        entry.smokes      = +entry.smokes;
    });

    return data;
}

function updateToolTip(plot, chosenXAxis, chosenYAxis) {
    let xlabel;
    let ylabel;

    switch(chosenXAxis) {
        case 'income':
            xlabel = 'Income';
            break;
        case 'age':
            xlabel = 'Age';
            break;
        case 'poverty':
        default:
            xlabel = 'Poverty';
            break;
    }

    switch(chosenYAxis) {
        case 'smokes':
            ylabel = 'Smokes';
            break
        case 'healthcare':
            ylabel = 'Healthcare';
            break
        case 'obesity':
        default:
            ylabel = 'Obesity';
            break;
    }

    var toolTip = d3.tip()
        .attr("class", "d3-tip")
        .offset([80, -60])
        .html(function(d) {
            return (`${d.state}<br>${xlabel} ${d[chosenXAxis]}<br>${ylabel} ${d[chosenYAxis]}`);
        });

    plot.circlesGroup.call(toolTip);

    plot.circlesGroup.on("mouseover", function(data) {
        toolTip.show(data, this);
    })
    // onmouseout event
    .on("mouseout", function(data, index) {
        toolTip.hide(data);
    });
}

function setLabels(plot) {
    const labelsGroup = plot.chartGroup.append("g")
        .attr("transform", `translate(${plot.chartArea.width / 2}, ${plot.chartArea.height + 20})`);

    const povertyLabel = labelsGroup.append("text")
        .attr("x", 0)
        .attr("y", 20)
        .attr("x-value", "poverty") // value to grab for event listener
        .classed("active", true)
        .text("In Poverty (%)")
        .on('click', labelHandler)
    ;

    const ageMedianLabel = labelsGroup.append("text")
        .attr("x", 0)
        .attr("y", 40)
        .attr("x-value", "age") // value to grab for event listener
        .classed("inactive", true)
        .text("Age (Median)")
        .on('click', labelHandler)
    ;

    const householdLabel = labelsGroup.append("text")
        .attr("x", 0)
        .attr("y", 60)
        .attr("x-value", "income") // value to grab for event listener
        .classed("inactive", true)
        .text("Household Income (Median)")
        .on('click', labelHandler)
    ;


    // append y axis
    const obeseLabel = plot.chartGroup.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - plot.margin.left)
        .attr("x", 0 - (plot.chartArea.height / 2))
        .attr("y-value", "obesity") // value to grab for event listener
        .attr("dy", "1em")
        .classed("axis-text", true)
        .classed("active", true)
        .text("Obese (%)")
        .on('click', labelHandler)
    ;

    const smokesLabel = plot.chartGroup.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - plot.margin.left + 20)
        .attr("x", 0 - (plot.chartArea.height / 2))
        .attr("y-value", "smokes") // value to grab for event listener
        .attr("dy", "1em")
        .classed("axis-text", true)
        .classed("inactive", true)
        .text("Smokes (%)")
        .on('click', labelHandler)
    ;

    const lacksHealthcareLabel = plot.chartGroup.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - plot.margin.left + 40)
        .attr("x", 0 - (plot.chartArea.height / 2))
        .attr("y-value", "healthcare") // value to grab for event listener
        .attr("dy", "1em")
        .classed("axis-text", true)
        .classed("inactive", true)
        .text("Lacks Healthcare (%)")
        .on('click', labelHandler)
    ;

    plot.labels = {povertyLabel, ageMedianLabel, householdLabel, obeseLabel, smokesLabel, lacksHealthcareLabel};
}

// function used for updating xAxis var upon click on axis label
function renderXAxes(newXScale, xAxis) {
  var bottomAxis = d3.axisBottom(newXScale);

  xAxis.transition()
    .duration(1000)
    .call(bottomAxis);

  return xAxis;
}

// function used for updating xAxis var upon click on axis label
function renderYAxes(newYScale, yAxis) {
  var leftAxis = d3.axisLeft(newYScale);

  yAxis.transition()
    .duration(1000)
    .call(leftAxis);

  return yAxis;
}

function labelHandler() {
    // get value of selection
    let yvalue = d3.select(this).attr("y-value");
    let xvalue = d3.select(this).attr("x-value");

    if (yvalue !== null && yvalue !== activeY) {
        // updated selected y value
        activeY = yvalue;
    }

    if (xvalue !== null && xvalue !== activeX) {
        // updated selected X value
        activeX = xvalue;
    }
    //console.log(activeX, activeY);return;

    const yLinearScale = yScale(plot, activeY);
    const xLinearScale = xScale(plot, activeX);

    // updates axis with transition
    plot.xAxis = renderXAxes(xLinearScale, plot.xAxis);
    plot.yAxis = renderYAxes(yLinearScale, plot.yAxis);

    // updates circles with new x values
    plot.circlesGroup = renderCircles(plot.circlesGroup, xLinearScale, activeX, yLinearScale, activeY);

    // updates tooltips with new info
    updateToolTip(plot, activeX, activeY);

    switch(activeX) {
        case 'income':
            setLabelInactive(plot.labels.povertyLabel);
            setLabelInactive(plot.labels.ageMedianLabel);
            setLabelActive(plot.labels.householdLabel);
            break;
        case 'age':
            setLabelInactive(plot.labels.povertyLabel);
            setLabelActive(plot.labels.ageMedianLabel);
            setLabelInactive(plot.labels.householdLabel);
            break;
        case 'poverty':
        default:
            setLabelActive(plot.labels.povertyLabel);
            setLabelInactive(plot.labels.ageMedianLabel);
            setLabelInactive(plot.labels.householdLabel);
            break;
    }

    switch(activeY) {
        case 'smokes':
            setLabelInactive(plot.labels.obeseLabel);
            setLabelActive(plot.labels.smokesLabel);
            setLabelInactive(plot.labels.lacksHealthcareLabel);
            break
        case 'healthcare':
            setLabelInactive(plot.labels.obeseLabel);
            setLabelInactive(plot.labels.smokesLabel);
            setLabelActive(plot.labels.lacksHealthcareLabel);
            break
        case 'obesity':
            setLabelActive(plot.labels.obeseLabel);
            setLabelInactive(plot.labels.smokesLabel);
            setLabelInactive(plot.labels.lacksHealthcareLabel);
        default:
            break;
    }
}

function setLabelActive(label) {
    label.classed("active", true)
    .classed("inactive", false);
}

function setLabelInactive(label) {
    label.classed("active", false)
    .classed("inactive", true);
}
